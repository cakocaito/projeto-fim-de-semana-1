const url = 'https://lobinhos.herokuapp.com/wolves'
const butCheck = document.querySelector("#lobosAdotados")
fetch(url)
.then(resp => resp.json())
.then(objeto => {
    // console.log(objeto.wolves)
    const lobosPagina = objeto.wolves.slice(0,10)
    for (let i = 0; i < 10; i++) {
        if (i % 2 == 0){
            cardLobo1(lobosPagina[i])
        }else {
            cardLobo2(lobosPagina[i])
        }
    }
})
butCheck.addEventListener("click", element => {
    // console.log("cliquei")
    if (event.target.checked){
        // console.log("é verdade")
        mostrarAdotados()
    }else {
        const zerarSection = document.querySelector(".section-4")
        zerarSection.innerHTML = ""
        fetch(url)
        .then(resp => resp.json())
        .then(objeto => {
            // console.log(objeto.wolves)
            const lobosPagina = objeto.wolves.slice(0,10)
            for (let i = 0; i < 10; i++) {
                if (i % 2 == 0){
                    cardLobo1(lobosPagina[i])
                }else {
                    cardLobo2(lobosPagina[i])
                }
            }
        
        })
    }
})

function cardLobo1(lobo) {
                const div = document.createElement('div')
                div.classList.add('lobo-1')
        
                const containerImg = document.createElement('div')
                containerImg.classList.add('container-img')
        
                const imagem = document.createElement('img')
                imagem.src = lobo.link_image
                containerImg.appendChild(imagem)
                div.appendChild(containerImg)
        
                const divInfo = document.createElement('div')
                divInfo.classList.add('info-1')
        
                const h3 = document.createElement('h3')
                h3.innerText = lobo.name
                divInfo.appendChild(h3)
        
                const span = document.createElement('span')
                span.innerText = `Idade: ${lobo.age} anos`
                divInfo.appendChild(span)
        
                const butaoAdotar = document.createElement("button")
                butaoAdotar.setAttribute("onclick", "pageLobo()")
                
                const link = document.createElement('a')
                // link.setAttribute("href", "./mostrarLobo.html")
                link.innerText = "Adotar"
                butaoAdotar.appendChild(link)
                divInfo.appendChild(butaoAdotar)
                
                const p = document.createElement('p')
                const id = document.createElement('p')
                p.innerText = lobo.description
                id.innerText = lobo.id
                id.style.display = "none"
                id.classList.add("id")
                divInfo.appendChild(p)
                divInfo.appendChild(id)
                
                div.appendChild(divInfo)
                // console.log(div)
        
                const secao = document.querySelector('.section-4')
                secao.appendChild(div)
}
        
function cardLobo2(lobo) {
            const div = document.createElement('div')
            div.classList.add('lobo-2')
        
            const containerImg = document.createElement('div')
            containerImg.classList.add('container-img')
        
            const imagem = document.createElement('img')
            imagem.src = lobo.link_image
            containerImg.appendChild(imagem)
            div.appendChild(containerImg)
        
            const divInfo = document.createElement('div')
            divInfo.classList.add('info-2')
        
            const h3 = document.createElement('h3')
            h3.innerText = lobo.name
            divInfo.appendChild(h3)
            
            const butaoAdotar = document.createElement("button")
            butaoAdotar.setAttribute("onclick", "pageLobo()")
            
            const link = document.createElement('a')
            // link.setAttribute("href", "./mostrarLobo.html")
            link.innerText = "Adotar"
            butaoAdotar.appendChild(link)
            divInfo.appendChild(butaoAdotar)
            
            const span = document.createElement('span')
            span.innerText = `Idade: ${lobo.age} anos`
            divInfo.appendChild(span)
            
            const p = document.createElement('p')
            const id = document.createElement('p')
            p.innerText = lobo.description
            id.innerText = lobo.id
            id.style.display = "none"
            id.classList.add("id")
            divInfo.appendChild(p)
            divInfo.appendChild(id)
            div.appendChild(divInfo)
            // console.log(div)
        
            const secao = document.querySelector('.section-4') 
            secao.appendChild(div)
}

function mostrarAdotados() {
    const urlAdotados = 'https://lobinhos.herokuapp.com/wolves/adopted'
    fetch(urlAdotados)
    .then(resp => resp.json())
    .then(objeto => {
        // console.log(objeto.wolves.length)
        const lobosAdotados = objeto.wolves
        const divSection = document.querySelector(".section-4")
        divSection.innerHTML = ""

        // const par = document.createElement("p")
        // par.innerText = "Lobo-3"
        // divSection.appendChild(par)
        for (let i = 0; i < lobosAdotados.length; i++) {
            if (i % 2 == 0){
                cardAdotado1(lobosAdotados[i])
            }else {
                cardAdotado2(lobosAdotados[i])
            }
        }
    })
}

function cardAdotado1(lobo) {
    const div = document.createElement('div')
    div.classList.add('lobo-1')

    const containerImg = document.createElement('div')
    containerImg.classList.add('container-img')

    const imagem = document.createElement('img')
    imagem.src = lobo.link_image
    containerImg.appendChild(imagem)
    div.appendChild(containerImg)

    const divInfo = document.createElement('div')
    divInfo.classList.add('info-1')

    const h3 = document.createElement('h3')
    h3.innerText = lobo.name
    divInfo.appendChild(h3)

    const span = document.createElement('span')
    span.innerText = `Idade: ${lobo.age} anos`
    divInfo.appendChild(span)

    const butaoAdotar = document.createElement("button")
    butaoAdotar.setAttribute("onclick", "pageLoboAdot()")
    butaoAdotar.innerText = "Adotado"
    butaoAdotar.classList.add("adotado")
    divInfo.appendChild(butaoAdotar)
    
    const p = document.createElement('p')
    const id = document.createElement('p')
    id.classList.add("id")
    p.innerText = lobo.description
    id.innerText = lobo.id
    divInfo.appendChild(p)
    divInfo.appendChild(id)
    div.appendChild(divInfo)
    // console.log(div)

    const secao = document.querySelector('.section-4')
    secao.appendChild(div)
}

function cardAdotado2(lobo) {
    const div = document.createElement('div')
    div.classList.add('lobo-2')

    const containerImg = document.createElement('div')
    containerImg.classList.add('container-img')

    const imagem = document.createElement('img')
    imagem.src = lobo.link_image
    containerImg.appendChild(imagem)
    div.appendChild(containerImg)

    const divInfo = document.createElement('div')
    divInfo.classList.add('info-2')

    const h3 = document.createElement('h3')
    h3.innerText = lobo.name
    divInfo.appendChild(h3)

    const butaoAdotar = document.createElement("button")
    butaoAdotar.setAttribute("onclick", "pageLoboAdot()")
    butaoAdotar.innerText = "Adotado"
    butaoAdotar.classList.add("adotado")
    divInfo.appendChild(butaoAdotar)

    const span = document.createElement('span')
    span.innerText = `Idade: ${lobo.age} anos`
    divInfo.appendChild(span)

    const p = document.createElement('p')
    const id = document.createElement('p')
    id.classList.add("id")
    p.innerText = lobo.description
    id.innerText = lobo.id
    divInfo.appendChild(p)
    divInfo.appendChild(id)
    div.appendChild(divInfo)
    // console.log(div)

    const secao = document.querySelector('.section-4') 
    secao.appendChild(div)
}