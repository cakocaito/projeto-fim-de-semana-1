botaoAdocao = document.querySelector(".botaoEnviar")

botaoAdocao.addEventListener("click",(e)=>{
    e.preventDefault();
    const url = 'http://lobinhos.herokuapp.com/wolves/adoption'
    const form = e.target.parentNode
    const ID = form.querySelector(".idAdotado")
    const nomeCadastro = form.querySelector("#nomeLobo")
    const email = form.querySelector("#emailAdocao")
    const idade = form.querySelector("#idadeLobo")
    console.log(ID.innerText)
    console.log(nomeCadastro.value)
    console.log(email.value)
    console.log(idade.value)
    if  ( nomeCadastro != '' || idade != '' || email != ''){
    
        const identidade = parseInt(ID.innerText)
        const idadeNumero  = parseInt(idade.value)
        let objAdocao = {
        "adoption":{ 
            name: nomeCadastro.value,
            age: idadeNumero,
            email: email.value, 
            wolf_id: identidade
        }
    }
        let fetchConfig = {
            method: "POST",
            headers: {"Content-Type": "application/json"},
            body: JSON.stringify(objAdocao)
        }
        fetch(url, fetchConfig) 
        .then(resp => {
            
            nomeCadastro.value = ''
            idade.value = ''
            email.value = ''
            alert("Adoção realizado com sucesso!")
            window.location.href = "index.html"
        })   
        .catch(error => console.warn(error))
    }else{
        alert("Algum campo de formulario esta vazio")
    }
        })

