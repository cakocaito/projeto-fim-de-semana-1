// const url = 'https://lobinhos.herokuapp.com/wolves'
function pageLoboAdot() {
    // console.log(event.target)
    // console.log(event.target.parentNode.parentNode)
    const divPai = event.target.parentNode.parentNode
    // // const nome = divPai.querySelector("h3")
    // // const idade = divPai.querySelector("span")
    // // const descricao = divPai.querySelector("p")
    const idLobo = divPai.querySelector(".id").innerText
    console.log(idLobo)
    console.log("Cliquei")
    
    localStorage.setItem("identificacaoAdot", idLobo)
    window.location.href = "MostraLoboAdotado.html"
    
}
const teste2 = document.querySelector(".vascaoDois")
if (teste2){
    mostrarLobinho()
}
function mostrarLobinho() {
    const idLobinho = localStorage.getItem("identificacaoAdot")
    // console.log(idLobinho)
    const urlLobo = `https://lobinhos.herokuapp.com/wolves/${idLobinho}`

    fetch(urlLobo)
    .then(resp => resp.json())
    .then(objeto => {
        const dadosLobo = objeto.wolf_adopted
        console.log(dadosLobo)
        console.log(dadosLobo.adoption)
        // localStorage.setItem("linkFoto", dadosLobo.link_image)
        // localStorage.setItem("loboAtual", dadosLobo.name)

        const sessaoAdd = document.querySelector('.vascaoDois')
        
        const div3 = document.querySelector(".infUsuario")
        const nomeUsuario = div3.querySelector("#usuario")
        nomeUsuario.innerText = dadosLobo.adoption.name
        const IdadeUsuario = div3.querySelector("#Emailusuario")
        IdadeUsuario.innerText = dadosLobo.adoption.email
        const AgeUsuario= div3.querySelector("#Ageusuario")
        AgeUsuario.innerText = dadosLobo.adoption.age

        const nomeLobo = document.createElement("h1")
        nomeLobo.classList.add("tituloMostrarLobo")
        nomeLobo.innerText = dadosLobo.name
        sessaoAdd.appendChild(nomeLobo)

        const divInfos = document.createElement("div")
        divInfos.classList.add("lobo-1")
        sessaoAdd.appendChild(divInfos)
        
        const containerImg = document.createElement("div")
        containerImg.classList.add("container-img")
        containerImg.classList.add("part2")
        divInfos.appendChild(containerImg)
        
        const img = document.createElement("img")
        img.src = dadosLobo.link_image
        containerImg.appendChild(img)

        // const adotar = document.createElement("button")
        // adotar.innerText = "Adotar"
        // adotar.id = "adotar"
        // containerImg.appendChild(adotar)
        
        // const excluir = document.createElement("button")
        // excluir.innerText = "Excluir"
        // excluir.id = "excluir"
        // containerImg.appendChild(excluir)

        const div2 = document.createElement("div")
        div2.classList.add("infos-1")
        divInfos.appendChild(div2)

        const spanIdade = document.createElement("span")
        spanIdade.innerText = dadosLobo.age
        div2.appendChild(spanIdade)
        
        const pDescri = document.createElement("p")
        pDescri.innerText = dadosLobo.description
        div2.appendChild(pDescri)

    })
}
