const url = `https://lobinhos.herokuapp.com/wolves` 
fetch(url)
.then(resp => resp.json())
.then(objeto =>{ 
   const listaDeLobos = objeto.wolves
   const doisLobos = listaDeLobos.slice(0,2)
    cardLobo1(doisLobos[0])
    cardLobo2(doisLobos[1])
})

function cardLobo1(lobo) {
    const div = document.createElement('div')
    div.classList.add('lobo-1')

    const containerImg = document.createElement('div')
    containerImg.classList.add('container-img')

    const imagem = document.createElement('img')
    imagem.src = lobo.link_image
    containerImg.appendChild(imagem)
    div.appendChild(containerImg)

    const divInfo = document.createElement('div')
    divInfo.classList.add('info-1')

    const h3 = document.createElement('h3')
    h3.innerText = lobo.name
    divInfo.appendChild(h3)

    const span = document.createElement('span')
    span.innerText = `Idade: ${lobo.age} anos`
    divInfo.appendChild(span)

    // const butaoAdotar = document.createElement("button")
    // butaoAdotar.setAttribute("onclick", "pageLobo()")
    
    // const link = document.createElement('a')
    // link.setAttribute("href", "./mostrarLobo.html")
    // link.innerText = "Adotar"
    // butaoAdotar.appendChild(link)
    // divInfo.appendChild(butaoAdotar)
    
    const p = document.createElement('p')
    const id = document.createElement('p')
    p.innerText = lobo.description
    id.innerText = lobo.id
    id.style.display = "none"
    id.classList.add("id")
    divInfo.appendChild(p)
    divInfo.appendChild(id)
    
    div.appendChild(divInfo)
    // console.log(div)

    const secao = document.querySelector('.section-4')
    secao.appendChild(div)
}

function cardLobo2(lobo) {
const div = document.createElement('div')
div.classList.add('lobo-2')

const containerImg = document.createElement('div')
containerImg.classList.add('container-img')

const imagem = document.createElement('img')
imagem.src = lobo.link_image
containerImg.appendChild(imagem)
div.appendChild(containerImg)

const divInfo = document.createElement('div')
divInfo.classList.add('info-2')

const h3 = document.createElement('h3')
h3.innerText = lobo.name
divInfo.appendChild(h3)

// const butaoAdotar = document.createElement("button")
// butaoAdotar.setAttribute("onclick", "pageLobo()")

// const link = document.createElement('a')
// link.setAttribute("href", "./mostrarLobo.html")
// link.innerText = "Adotar"
// butaoAdotar.appendChild(link)
// divInfo.appendChild(butaoAdotar)

const span = document.createElement('span')
span.innerText = `Idade: ${lobo.age} anos`
divInfo.appendChild(span)

const p = document.createElement('p')
const id = document.createElement('p')
p.innerText = lobo.description
id.innerText = lobo.id
id.style.display = "none"
id.classList.add("id")
divInfo.appendChild(p)
divInfo.appendChild(id)
div.appendChild(divInfo)
// console.log(div)

const secao = document.querySelector('.section-4') 
secao.appendChild(div)
}