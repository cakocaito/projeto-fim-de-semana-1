function pageLobo() {
    // console.log(event.target)
    console.log(event.target.parentNode.parentNode)
    const divPai = event.target.parentNode.parentNode
    // const nome = divPai.querySelector("h3")
    // const idade = divPai.querySelector("span")
    // const descricao = divPai.querySelector("p")
    const idLobo = divPai.querySelector(".id").innerText
    console.log(idLobo)
    
    
    localStorage.setItem("identificacao", idLobo)
    window.location.href = "mostrarLobo.html"
    
}

const teste = document.querySelector(".vascao")
if (teste){
    mostrarLobinho()
}

function mostrarLobinho() {
    const idLobinho = localStorage.getItem("identificacao")
    // console.log(idLobinho)
    const urlLobo = `https://lobinhos.herokuapp.com/wolves/${idLobinho}`

    fetch(urlLobo)
    .then(resp => resp.json())
    .then(objeto => {
        const dadosLobo = objeto.wolf
        // console.log(dadosLobo)
        // localStorage.setItem("linkFoto", dadosLobo.link_image)
        // localStorage.setItem("loboAtual", dadosLobo.name)

        const sessaoAdd = document.querySelector('.vascao')

        const nomeLobo = document.createElement("h1")
        nomeLobo.classList.add("tituloMostrarLobo")
        nomeLobo.innerText = dadosLobo.name
        sessaoAdd.appendChild(nomeLobo)

        const divInfos = document.createElement("div")
        divInfos.classList.add("lobo-1")
        sessaoAdd.appendChild(divInfos)
        
        const containerImg = document.createElement("div")
        containerImg.classList.add("container-img")
        containerImg.classList.add("part2")
        divInfos.appendChild(containerImg)
        
        const img = document.createElement("img")
        img.src = dadosLobo.link_image
        containerImg.appendChild(img)

        const adotar = document.createElement("button")
        adotar.innerText = "Adotar"
        adotar.id = "adotar"
        containerImg.appendChild(adotar)
        
        const excluir = document.createElement("button")
        excluir.innerText = "Excluir"
        excluir.id = "excluir"
        containerImg.appendChild(excluir)

        const div2 = document.createElement("div")
        div2.classList.add("infos-1")
        divInfos.appendChild(div2)

        const spanIdade = document.createElement("span")
        spanIdade.innerText = dadosLobo.age
        div2.appendChild(spanIdade)
        
        const pDescri = document.createElement("p")
        pDescri.innerText = dadosLobo.description
        div2.appendChild(pDescri)

        deleta()

        NewPag()
    })
}



function deleta(){
    const deletar = document.querySelector("#excluir")
    deletar.addEventListener("click", ()=> {

        const idLobinho = localStorage.getItem("identificacao")
        console.log(idLobinho)
        const url = `https://lobinhos.herokuapp.com/wolves/${idLobinho}`    
        let fetchConfig = {
            method:"DELETE"
        }
        fetch(url, fetchConfig)
        .then(
            alert("Lobo excluido com sucesso"),
            window.location.href = "listaLobos.html"
        )
        .catch(error => console.warn(error))
    })
}

function NewPag(){
    const adotar = document.querySelector("#adotar")
    adotar.addEventListener("click", ()=> {
        window.location.href ="adocao.html"
        // const idLobinho = localStorage.getItem("identificacao")
        // const imagemLoboAtual = localStorage.getItem("linkFoto")
        // const nomeLoboAtual = localStorage.getItem("loboAtual")'
        // adotarLobo(idLobinho)
        // console.log(idLobinho,imagemLoboAtual,nomeLoboAtual)
    })
}
const testeAdot = document.querySelector(".adocao")
if (testeAdot){
    const idAtual = localStorage.getItem("identificacao")
    adotarLobo(idAtual)    
} 
function adotarLobo(idLobinho){
    url = `https://lobinhos.herokuapp.com/wolves/${idLobinho}`
    fetch(url)
    .then(resp => resp.json())
    .then(objeto =>{
        const lobao = objeto.wolf
        const nome = document.querySelector(".NomeDeAdotado")
        nome.innerText = lobao.name
        const imagemLobao = document.querySelector(".imagemAdocao")
        imagemLobao.style.backgroundImage = `url(${lobao.link_image})`
        const idLobo = document.querySelector(".idAdotado")
        idLobo.innerText = lobao.id
    })
}


